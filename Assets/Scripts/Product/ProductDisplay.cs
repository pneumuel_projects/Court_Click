﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProductDisplay : MonoBehaviour {

    public Product product;

    public Text nameText;
    public Text priceText;
    public Text speedText;

    public Image btnBackground;
    public Slider slider;
    public Button btn;
    public Image buttonImage;

    void Awake () {

        nameText.text = product.name;
        speedText.text = product.speed.ToString();
        priceText.text = product.price.ToString();
        btnBackground.sprite = product.btnBackground;
        slider = product.slider;

        btn = product.button;
        buttonImage.sprite = product.buttonImage;
    }

    public Product getProduct()
    {
        return product;
    }

    
}
