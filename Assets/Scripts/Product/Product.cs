﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu (fileName = "New Product", menuName = "Product")]
public class Product : ScriptableObject {

    public new string name;
    
    public Button button;
    public Sprite buttonImage;
    public Slider slider;
    public Sprite btnBackground;

    public ulong price;
    public int speed;

}
