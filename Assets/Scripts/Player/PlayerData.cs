﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerData {


    public ulong money = 0;
    

    public PlayerData(Player player)
    {
        money = player.Money;
    }
}
